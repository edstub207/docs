### Feature flags are discouraged from security merge requests

Feature flags are used in regular development to reduce risk and
to do controlled testing. Engineers might be tempted to use them
on security merge requests, but this approach has several inconveniences:

* A self-hosted instance might disable the flag and be vulnerable.
* GitLab.com will be vulnerable if the fix doesn't work as expected.
  * If this happens, GitLab.com will likely need to be hotpatched,
    putting additional workload on the Infrastructure team.
  * Alternatively, Release Managers and AppSec can decide to publish the security
    release with the vulnerability exposed and follow up with a critical security release. 
    This option is no better, it also adds workload to the Release Managers and engineers involved. 
* There's no process for handling feature flags in backports. If the feature flag
  is backported, it's less likely to be cleaned up once it's no longer necessary.
* Feature flags should be short-lived: once they've been tested 
  they should be removed from the codebase. Security releases are time-sensitive,
  and security merge requests are deployed within a limited timeframe which doesn't
  give enough time to introduce a feature flag, test it, and remove it. 
